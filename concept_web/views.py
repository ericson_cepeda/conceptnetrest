# Create your views here.
from django.http import HttpResponse

import json
from settings import SIMILARITY_FUNCTION
from csc.nl import get_nl
import itertools as it

def home(request):
    # get corresponding results
    context = {}

    uri_request = request.GET

    if "w1" in uri_request and "w2" in uri_request:
        sentence1 = request.GET['w2'].decode('utf8')
        sentence2 = request.GET['w1'].decode('utf8')
        context['sentence1'], context['sentence2'], context["similarity"] = _get_concept_similarity(sentence1, sentence2)
    return HttpResponse(json.dumps(context), mimetype="application/json")


def _get_concept_similarity(sentence1, sentence2):

        # We can now do natural language processing in English.

        def recognized_words(sentence):
            """Find all of the words in the sentence that are also concepts in analogyspace"""
            en_nl = get_nl('en')
            return list(set(en_nl.extract_concepts(sentence, max_words=1, check_conceptnet=True)))

        vector1, vector2 = [recognized_words(sentence) for sentence in (sentence1, sentence2)]

        similarity = SIMILARITY_FUNCTION

        scores = []
        # generate permutations of nodes in list
        penalties = 0
        for pair in it.product(vector1, vector2):
            try:
                pair_similarity = similarity.entry_named(pair[0], pair[1])
                if pair_similarity < 0:
                    penalties += 1
                else:
                    scores.append(pair_similarity)
            except:
                scores.append(0.0)

        result = (sum(scores)-penalties*0.01)/max(len(vector1), len(vector2))
        return vector1, vector2, result