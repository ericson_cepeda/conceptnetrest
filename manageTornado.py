#!/usr/bin/env python
from tornado.options import options, define, parse_command_line
from tornado.httpserver import HTTPServer
from tornado.process import *
import tornado.ioloop, tornado.wsgi, os, django.core.handlers.wsgi, sys

try: import simplejson as json
except ImportError: import json

sys.path.append('/home/www/concept/')

#define('ports', type=array, default=[8000, 8001])
define('port', type=int, default=8000)

def main():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
    port = options.port


    new_port = port
    os.popen("fuser -k {0}/tcp".format(new_port)).read()
    run_one(new_port)

def run_one(port):
    application = django.core.handlers.wsgi.WSGIHandler()
    container = tornado.wsgi.WSGIContainer(application)

    tornado_app = tornado.web.Application(
        [
            ('.*', tornado.web.FallbackHandler, dict(fallback=container)),
        ]
    )

    new_server = HTTPServer(tornado_app)
    new_server.listen(port)

    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()
