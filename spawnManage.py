#!/usr/bin/env python
import os
import sys

def main():
    print os.popen("fuser -k 8000/tcp").read()
    print str(os.spawnv(os.P_NOWAIT, sys.executable, ("python", "manageTornado.py")))

if __name__ == "__main__":
    main()
